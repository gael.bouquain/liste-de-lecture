import {StyleSheet} from "react-native";

export let background = "#202125"
export let textColor = "#007daa"


let style = StyleSheet.create({
    box: {
        alignItems: "center",
        justifyContent: "center",
        fontFamily: "Manjari-Bold",
        backgroundColor: background
    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        fontFamily: "Manjari-Bold",
        backgroundColor: background
    },
    Text: {
        color: textColor,
        fontSize: 40,
        margin: 10,
        marginTop: 20,
        fontFamily: "Manjari-Bold",
        borderRadius: 200
    },
    Symbole: {
        color: textColor,
        fontSize: 40,
        margin: 0,
        marginTop: -20,
        fontFamily: "Manjari-Bold"
    },
    text: {
        color: textColor,
        fontSize: 30,
        margin: 10,
        fontFamily: "Manjari-Bold"
    },
    Number: {
        color: "black",
        fontSize: 30,
        marginLeft: -45,
        marginTop: -7,
        fontFamily: "Manjari-Bold"
    },
    element: {
        width: 100,
        height: 100,
        borderWidth: 5,
        borderRadius: 10,
        borderColor: "black",
        alignItems: "center"
    },
    button: {
        margin: 25,
        padding: 0,
        borderRadius: 300,
        paddingHorizontal: 10,
        elevation: 2
    },
    label: {
        color: '#125a82',
        fontFamily: "Manjari-Bold",
        fontSize: 20
    },
    drawer: {
        height: 130,
        width: 130,
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: 30,
        marginBottom: 20
    },
    textOne: {
        position: "absolute",
        left: 5,
        top: 85,
        alignItems: 'center',
        color: "#ae7156",
        margin: 0,
        fontSize: 20,
        margin: 10,
        fontFamily: "Manjari-Bold"
    },
    textTwo: {
        fontSize: 20,
        color: textColor,
    },
    textInput: {
        fontSize: 20,
        borderRadius: 10,
        color: textColor,
        borderColor: textColor,
        fontFamily: "Manjari-Bold",
    },
    textInputAdd: {
        fontSize: 20,
        borderRadius: 20,
        color: textColor,
        borderColor: textColor,
        fontFamily: "Manjari-Bold",
        borderWidth: 1
    }
})

export default style
