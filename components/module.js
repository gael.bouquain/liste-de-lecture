import React from "react"
import { View, Text, Dimensions, Image } from "react-native"
import style from "./style"
import ProgressLabel from "react-progress-label"
import { TouchableNativeFeedback } from "react-native"

function pourcent(pageNumber, pageRead) {
	return Math.round((pageRead * 100) / pageNumber)
}

let width = Dimensions.get("window").width
let height = Dimensions.get("window").height

class Module extends React.Component {
	render() {
		return (
			<View
				style={[
					style.box,
					this.props.style,
					{
						height: 140,
						elevation: 15,
						marginTop: 20,
						borderRadius: 20,
						width: width - 20,
						alignItems: "center",
						alignSelf: "center",
						justifyContent: "center",
						marginBottom: 20,
					},
				]}
			>
				<Text style={[style.text, { marginTop: -60, marginBottom: 0 }]}>
					{this.props.bookName}
				</Text>

				<Text style={[style.textOne, { top: 50 }]}>
					{`${this.props.pageNumber}`}
					<Text style={style.textTwo}>{" pages"}</Text>
				</Text>
				<Text style={style.textOne}>
					{`${this.props.pagesRead}`}
					<Text style={style.textTwo}>{" pages lues"}</Text>
				</Text>

				<View
					style={{
						alignItems: "center",
						position: "absolute",
						justifyContent: "center",
						top: -10,
						left: 190,
					}}
				>
					<ProgressLabel
						progress={pourcent(
							this.props.pageNumber,
							this.props.pagesRead
						)}
						progressColor="#1b4e71"
						progressWidth={10}
						trackWidth={150}
						cornersWidth={5}
						text={`${pourcent(
							this.props.pageNumber,
							this.props.pagesRead
						)}%`}
						textProps={{
							x: "50%",
							y: "54%",
							textAnchor: "middle",
							style: {
								fontSize: 20,
								fontWeight: "500",
								fill: "#1b4e71",
								fontFamily: "Manjari-Bold",
							},
						}}
					/>
				</View>
				<View style={{ position: "absolute", right: 10, top: 10 }}>
					<TouchableNativeFeedback
						onPress={() => {
							console.log("onpress")
							this.props.delete()
						}}
					>
						<Image
							style={{ width: 35, height: 35 }}
							source={require(`../media/delete.png`)}
						/>
					</TouchableNativeFeedback>
				</View>

				<View style={{ position: "absolute", right: 120, top: 90 }}>
					<TouchableNativeFeedback onPress={this.props.add}>
						<Image
							style={{ width: 35, height: 35 }}
							source={require(`../media/plus.png`)}
						/>
					</TouchableNativeFeedback>
				</View>

				<View style={{ position: "absolute", top: 5, left: 10 }}>
					<Image
						style={{ height: 55, width: 45 }}
						source={this.props.icon}
					/>
				</View>
			</View>
		)
	}
}

export default Module
