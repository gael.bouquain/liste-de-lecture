import React from "react"
import { StatusBar, View } from "react-native"
import "react-native-gesture-handler"
import style from "./components/style"
import Home from "./screens/home"

export default function App() {
	return (
		<View style={style.container}>
			<StatusBar hidden={true} />
			<Home />
		</View>
	)
}
