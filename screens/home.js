import React from "react"
import {
	View,
	Text,
	Image,
	Alert,
	TextInput,
	Dimensions,
	ScrollView,
	TouchableNativeFeedback,
	ToastAndroid,
} from "react-native"
import Module from "../components/module"
import style, { background } from "../components/style"
import { DarkModalCustom } from "react-native-dark-modal"
import AsyncStorage from "@react-native-community/async-storage"
import LinearGradient from "react-native-linear-gradient"

let blue = ["#003251", "#00415e", "#005069", "#006072", "#006f79"]

let arrayIcon = [
	require("../media/hearth.png"),
	require("../media/horror.png"),
	require("../media/mask.png"),
	require("../media/book.png"),
	require("../media/rock.png"),
	require("../media/rose.png"),
	require("../media/UFO.png"),
	require("../media/unicorn.png"),
]

class Home extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			modalVisible: false,
			modalVisibleAdd: false,
			modalVisibleDelete: false,
			titleOfBook: null,
			numberOfPages: 0,
			numberOfPagesRead: 0,
			addIndex: null,
			deleteIndex: 0,
			icon: null,
			t: [],
		}
	}

	async setData(storage_Key, stored_value) {
		try {
			await AsyncStorage.setItem(storage_Key, stored_value)
		} catch (e) {
			Alert.alert(`${e}`)
		}
	}

	getData = async (storage_Key) => {
		let value = null
		try {
			value = await AsyncStorage.getItem(storage_Key)
		} catch (e) {
			Alert.alert("il y a eu un petit problème")
		}
		return value
	}

	setModalVisible() {
		this.setState({ modalVisible: !this.state.modalVisible })
	}

	setModalVisiblePlus() {
		this.setState({ modalVisibleAdd: !this.state.modalVisibleAdd })
	}

	setModalVisibleDelete() {
		this.setState({ modalVisibleDelete: !this.state.modalVisibleDelete })
	}

	setIcon(icon) {
		this.state.icon = icon
		ToastAndroid.show("C'est bon, ton icon est appliquée", 1500)
	}

	MyModalCreateBook = (index) => {
		return (
			<DarkModalCustom
				width={340}
				height={550}
				visible={this.state.modalVisible}
			>
				<ScrollView>
					<View style={{ marginTop: 25 }}>
						<TextInput
							maxLength={15}
							onChangeText={(text) =>
								(this.state.titleOfBook = text)
							}
							underlineColorAndroid="#000000"
							textAlign={"center"}
							placeholderTextColor={"#007daa80"}
							placeholder={`Le nom de ton livre `}
							style={style.textInput}
						/>
						<TextInput
							maxLength={3}
							onChangeText={(text) =>
								(this.state.numberOfPages = text || 0)
							}
							underlineColorAndroid="#000000"
							keyboardType={"phone-pad"}
							textAlign={"center"}
							placeholderTextColor={"#007daa80"}
							placeholder={`Le nombre de pages\nde ton livre `}
							style={style.textInput}
						/>
						<TextInput
							maxLength={3}
							onChangeText={(text) =>
								(this.state.numberOfPagesRead = text || 0)
							}
							underlineColorAndroid="#000000"
							keyboardType={"phone-pad"}
							textAlign={"center"}
							placeholderTextColor={"#007daa80"}
							placeholder={`Le nombres de pages\nque tu as lu `}
							style={style.textInput}
						/>
					</View>
					<View
						style={{
							flexDirection: "row",
							flexWrap: "wrap",
							justifyContent: "space-evenly",
						}}
					>
						{arrayIcon.map((icon, i) => {
							return (
								<TouchableNativeFeedback
									onPressIn={() => this.setIcon(icon)}
									key={"tb" + i}
								>
									<Image
										style={{
											height: 80,
											width: 63,
											marginLeft: 8,
											marginRight: 8,
											marginTop: 8,
										}}
										source={icon}
									/>
								</TouchableNativeFeedback>
							)
						})}

						{/* <TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[2])}
						>
							<Image
								style={{
									height: 80,
									width: 63,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[2]}
							/>
						</TouchableNativeFeedback>
						<TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[3])}
						>
							<Image
								style={{
									height: 80,
									width: 63,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[3]}
							/>
						</TouchableNativeFeedback>
						<TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[4])}
						>
							<Image
								style={{
									height: 80,
									width: 63,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[4]}
							/>
						</TouchableNativeFeedback>
						<TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[5])}
						>
							<Image
								style={{
									height: 80,
									width: 68,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[5]}
							/>
						</TouchableNativeFeedback>
						<TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[6])}
						>
							<Image
								style={{
									height: 80,
									width: 63,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[6]}
							/>
						</TouchableNativeFeedback>
						<TouchableNativeFeedback
							onPressIn={() => this.setIcon(arrayIcon[7])}
						>
							<Image
								style={{
									height: 80,
									width: 63,
									marginLeft: 8,
									marginRight: 8,
									marginTop: 8,
								}}
								source={arrayIcon[7]}
							/>
						</TouchableNativeFeedback> */}
					</View>
				</ScrollView>
				<View style={{ flexDirection: "row" }}>
					<TouchableNativeFeedback
						onPress={() => this.setModalVisible()}
					>
						<LinearGradient
							style={style.button}
							colors={blue}
							start={{ x: 0, y: 0 }}
							end={{ x: 1, y: 0 }}
						>
							<Text style={[style.text, { marginBottom: 4 }]}>
								annuler
							</Text>
						</LinearGradient>
					</TouchableNativeFeedback>
					<TouchableNativeFeedback
						onPress={() => this.createNewBook(index)}
					>
						<LinearGradient
							style={style.button}
							colors={blue}
							start={{ x: 0, y: 0 }}
							end={{ x: 1, y: 0 }}
						>
							<Text style={[style.text, { marginBottom: 4 }]}>
								Ok
							</Text>
						</LinearGradient>
					</TouchableNativeFeedback>
				</View>
			</DarkModalCustom>
		)
	}

	MyModalAddToBook() {
		return (
			<DarkModalCustom
				width={300}
				height={300}
				visible={this.state.modalVisibleAdd}
			>
				<View>
					<TextInput
						maxLength={3}
						onChangeText={(text) =>
							(this.state.numberOfPagesRead = text)
						}
						keyboardType={"phone-pad"}
						textAlign={"center"}
						placeholderTextColor={"#007daa80"}
						placeholder={`À quelle page en\nes-tu rendu ?`}
						style={[
							style.textInputAdd,
							{ marginTop: 40, width: 200 },
						]}
					/>
				</View>
				<TouchableNativeFeedback
					onPress={() => this.createNewBook(this.state.addIndex)}
				>
					<LinearGradient
						style={style.button}
						colors={blue}
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 0 }}
					>
						<Text style={[style.text, { marginBottom: 4 }]}>
							Ok
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>
				<TouchableNativeFeedback
					onPress={() => this.setModalVisiblePlus()}
				>
					<LinearGradient
						style={style.button}
						colors={blue}
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 0 }}
					>
						<Text style={[style.text, { marginBottom: 4 }]}>
							Annuler
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>
			</DarkModalCustom>
		)
	}

	MyModalDelete = () => {
		return (
			<DarkModalCustom
				width={300}
				height={300}
				visible={this.state.modalVisibleDelete}
			>
				<Text
					style={[
						style.text,
						{ alignSelf: "center", textAlign: "center" },
					]}
				>{`Est tu sur de vouloires supprimer ce livre ?`}</Text>
				<TouchableNativeFeedback
					onPress={() => this.setModalVisibleDelete()}
				>
					<LinearGradient
						style={style.button}
						colors={blue}
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 0 }}
					>
						<Text style={[style.text, { marginBottom: 4 }]}>
							Non
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>
				<TouchableNativeFeedback
					onPress={() => {
						this.delete()
					}}
				>
					<LinearGradient
						style={style.button}
						colors={blue}
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 0 }}
					>
						<Text style={[style.text, { marginBottom: 4 }]}>
							Oui
						</Text>
					</LinearGradient>
				</TouchableNativeFeedback>
			</DarkModalCustom>
		)
	}

	componentDidMount = async () => {
		let tableau =
			(await JSON.parse(await AsyncStorage.getItem("array"))) ||
			this.state.t

		if (tableau && tableau[0]?.title) {
			this.setState({ t: tableau })
		} else if (!tableau || (tableau && !tableau[0]?.title)) {
			this.setModalVisible()
			AsyncStorage.clear()
		}
	}

	add = (index) => {
		this.setState({ addIndex: index })
		this.setModalVisiblePlus()
	}

	deleteShow = (index) => {
		this.state.deleteIndex = index
		this.setModalVisibleDelete()
	}

	delete = async () => {
		let newT = this.state.t.filter((x, i) => i !== this.state.deleteIndex)
		this.setState({ t: newT })
		let tForData = JSON.stringify(newT)
		await this.setData("array", tForData)
		this.componentDidMount()
		this.setModalVisibleDelete()
	}

	async createNewBook(index) {
		// ? addPage
		if (this.state.modalVisibleAdd == true) {
			let newT = this.state.t.map((x, i) => {
				if (i === index) {
					return {
						...this.state.t[index],
						pagesLues: parseInt(this.state.numberOfPagesRead),
					}
				} else {
					return x
				}
			})

			this.setState({ t: newT })
			let thisTForData = JSON.stringify(newT)
			await this.setData("array", thisTForData)
			this.setModalVisiblePlus()
		} else {
			let newBook = {
				title: this.state.titleOfBook,
				pages: this.state.numberOfPages,
				pagesLues: this.state.numberOfPagesRead,
				icon: this.state.icon,
			}
			let newT = this.state.t.concat([newBook])
			this.setState({ t: newT })
			let tForData = JSON.stringify(newT)
			await this.setData("array", tForData)
			this.setModalVisible()

			this.state.titleOfBook = null
			this.state.numberOfPages = 0
			this.state.numberOfPagesRead = 0
			this.state.icon = null
		}
	}

	render() {
		return (
			<View
				style={{
					flex: 1,
					flexWrap: "wrap",
					flexDirection: "row",
					backgroundColor: background,
					justifyContent: "center",
				}}
			>
				{this.MyModalCreateBook()}

				{this.MyModalAddToBook()}

				{this.MyModalDelete()}

				<ScrollView
					style={{
						width: Dimensions.get("screen").width,
						height: Dimensions.get("screen").height,
					}}
				>
					{this.state.t.map((v, index) => (
						<Module
							delete={() => this.deleteShow(index)}
							add={() => this.add(index)}
							icon={v.icon}
							bookName={v.title}
							pageNumber={v.pages}
							pagesRead={v.pagesLues}
							key={"item_" + v.title}
						></Module>
					))}
				</ScrollView>

				<View
					style={{
						width: 75,
						height: 75,
						position: "absolute",
						elevation: 10,
						bottom: 10,
					}}
				>
					<TouchableNativeFeedback
						onPress={() => this.setModalVisible()}
					>
						<Image
							source={require("../media/plus.png")}
							style={{ width: 77, height: 77 }}
						/>
					</TouchableNativeFeedback>
				</View>
			</View>
		)
	}
}

export default Home
